% SphericalGrav is a test script for OrbitProp
%
% This is script shows the differences between the different gravity
% models.

%--- CODE ---%

%Clean up
clc
clear all
close all

%Add paths
addpath('..')
Add_AL_Dependencies();

%--- Initial state vector and model configuration ---%
%Earth parameters
Re = 6378136.49; %Equatorial Earth radius m [source: SMAD 3rd edition]
mu=398600.441e9;  %GM Earth m3/s2 [source: SMAD 3rd edition]

%Initial conditions
h = 200*1e3; %Initial altitude in m
i = 70; %Inclination
v0 = sqrt(mu/(Re+h)); %Initial velocity
x0 = [Re+h,0,0,0,v0*cosd(i),v0*sind(i)]; %Initial vector state
t0 = cspice_str2et( '2010 JAN 1 12:00:00' ); %Initial time of integration
tf = t0 + 90*60; %Final time of integration (180 minutes later)
dt = 60; % Integration solution output time step.\

%Reference frame
data.prop.Reference.CentreBody='EARTH';
data.prop.Reference.Frame='J2000';

%Get x0 in Reference.Frame
Rot = cspice_pxform( data.prop.Reference.Frame, 'ITRF93', t0);
x0(1:3)=Rot\x0(1:3)';
x0(4:6)=Rot\x0(4:6)';

%Format initial conditions
data.prop.x0=x0;
data.prop.t=t0:dt:tf;

%Add waitbar
data.prop.waitbar=true;

%--- Iterate for different Gravity models---%
%Spherical gravity
data.models={@GravSpherical};
%Integrate
[data] = OrbitProp(data);
t=data.sol.t;
x=data.sol.x.(data.prop.Reference.CentreBody).(data.prop.Reference.Frame);
% Convert position and velocity to EARTH Reference frame
REF_EARTH=cspice_spkezr( 'EARTH', t', data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody)';
EARTH_SC=x(:,1:6)-REF_EARTH*1e3;
% Convert position to ECEF (ITRF93) using the SPICE routine
for i=1:length(t)
    Rot = cspice_pxform( data.prop.Reference.Frame, 'ITRF93', t(i));
    r(i,1:3)=Rot*EARTH_SC(i,1:3)';
    v(i,1:3)=Rot*EARTH_SC(i,4:6)';
end
% Latitude and longitude
[lat_rad,lon_rad,h]=ecef2geodetic(r(:,1),r(:,2),r(:,3),referenceEllipsoid('wgs84'));
%Save relevant variables
hsphe=h/1e3;
tsphe=t-t0;

%J4 gravity
data.models={@GravJ4};
data.GravJ4.EarthFrame = 'ITRF93';
%Integrate
[data] = OrbitProp(data);
t=data.sol.t;
x=data.sol.x.(data.prop.Reference.CentreBody).(data.prop.Reference.Frame);
% Convert position and velocity to EARTH Reference frame
REF_EARTH=cspice_spkezr( 'EARTH', t', data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody)';
EARTH_SC=x(:,1:6)-REF_EARTH*1e3;
% Convert position to ECEF (ITRF93) using the SPICE routine
for i=1:length(t)
    Rot = cspice_pxform( data.prop.Reference.Frame, 'ITRF93', t(i));
    r(i,1:3)=Rot*EARTH_SC(i,1:3)';
    v(i,1:3)=Rot*EARTH_SC(i,4:6)';
end
% Latitude and longitude
[lat_rad,lon_rad,h]=ecef2geodetic(r(:,1),r(:,2),r(:,3),referenceEllipsoid('wgs84'));
%Save relevant variables
hJ4=h/1e3;
tJ4=t-t0;

%EGM96 gravity
data.models={@GravEGM96};
data.GravEGM96.EarthFrame = 'ITRF93';
%Integrate
[data] = OrbitProp(data);
t=data.sol.t;
x=data.sol.x.(data.prop.Reference.CentreBody).(data.prop.Reference.Frame);
% Convert position and velocity to EARTH Reference frame
REF_EARTH=cspice_spkezr( 'EARTH', t', data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody)';
EARTH_SC=x(:,1:6)-REF_EARTH*1e3;
% Convert position to ECEF (ITRF93) using the SPICE routine
for i=1:length(t)
    Rot = cspice_pxform( data.prop.Reference.Frame, 'ITRF93', t(i));
    r(i,1:3)=Rot*EARTH_SC(i,1:3)';
    v(i,1:3)=Rot*EARTH_SC(i,4:6)';
end
% Latitude and longitude
[lat_rad,lon_rad,h]=ecef2geodetic(r(:,1),r(:,2),r(:,3),referenceEllipsoid('wgs84'));
%Save relevant variables
hEGM96=h/1e3;
tEGM96=t-t0;

%EGM2008 gravity
data.models={@GravEGM2008};
data.GravEGM2008.EarthFrame = 'ITRF93';
%Integrate
[data] = OrbitProp(data);
t=data.sol.t;
x=data.sol.x.(data.prop.Reference.CentreBody).(data.prop.Reference.Frame);
% Convert position and velocity to EARTH Reference frame
REF_EARTH=cspice_spkezr( 'EARTH', t', data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody)';
EARTH_SC=x(:,1:6)-REF_EARTH*1e3;
% Convert position to ECEF (ITRF93) using the SPICE routine
for i=1:length(t)
    Rot = cspice_pxform( data.prop.Reference.Frame, 'ITRF93', t(i));
    r(i,1:3)=Rot*EARTH_SC(i,1:3)';
    v(i,1:3)=Rot*EARTH_SC(i,4:6)';
end
% Latitude and longitude
[lat_rad,lon_rad,h]=ecef2geodetic(r(:,1),r(:,2),r(:,3),referenceEllipsoid('wgs84'));
%Save relevant variables
hEGM2008=h/1e3;
tEGM2008=t-t0;

%--- Plot Altitudes ---%
figure(1)
plot(tsphe/60,hsphe,tJ4/60,hJ4,tEGM96/60,hEGM96,tEGM2008/60,hEGM2008)
legend('Spherical','J4','EGM96','EGM2008')
xlabel('Time [min]')
ylabel('Altitude [km]')
