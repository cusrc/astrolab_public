% Drag_J4 is a test script for OrbitProp
%
% This is script uses the a gravity model with up to the J4 harmonics. Also
% it includes drag using a simple ballistic coefficient
%
% The integration stops when the spacecraft re-enters (assumed to be at 100
% km).

%--- CODE ---%

%Clean up
clc
clear all
close all

%Add paths
addpath('..')
Add_AL_Dependencies();

%--- Initial state vector and model configuration ---%
%Earth parameters
Re = 6378136.49; %Equatorial Earth radius m [source: SMAD 3rd edition]
mu=398600.441e9;  %GM Earth m3/s2 [source: SMAD 3rd edition]

%Initial conditions
h = 250*1e3; %Initial altitude in m
i = 70; %Inclination
v0 = sqrt(mu/(Re+h)); %Initial velocity
x0 = [Re+h,0,0,0,v0*cosd(i),v0*sind(i)]; %Initial vector state
t0 = cspice_str2et( '2010 JAN 1 12:00:00' ); %Initial time of integration
tf = inf; %Integrate until re-entry

%Reference frame
data.prop.Reference.CentreBody='EARTH';
data.prop.Reference.Frame='J2000';

%Get x0 in Reference.Frame
Rot = cspice_pxform( data.prop.Reference.Frame, 'ITRF93', t0);
x0(1:3)=Rot\x0(1:3)';
x0(4:6)=Rot\x0(4:6)';

%Format initial conditions
data.prop.x0=x0;
data.prop.t=[t0,tf];

%Models
data.models={@GravJ4,@ConstantBCCoWind};

%Configure GravJ4 model
data.GravJ4.EarthFrame = 'ITRF93';

%Configure ConstantBC model
data.ConstantBCCoWind.BC=2/(0.1*0.1*2.5); %Ballistic coefficient
data.ConstantBCCoWind.atmos_model=@nrlmsise00;
data.ConstantBCCoWind.wind_model = @NoWind;
data.ConstantBCCoWind.EarthFrame = 'ITRF93';

%Configure nrlmsise00
data.nrlmsise00.solar_activity = 'Medium'; %Level of solar activity
data.nrlmsise00.EarthFrame = 'ITRF93';

%Event
data.event = @EvAlt;
data.EvAlt.h = 100e3;

%--- Call function ---%
[data] = OrbitProp(data);

%--- Post process ---%
t=data.sol.t;
x=data.sol.x.(data.prop.Reference.CentreBody).(data.prop.Reference.Frame);

% Convert into EARTH coordinates
% Convert position and velocity to EARTH Reference frame
REF_EARTH=cspice_spkezr( 'EARTH', t', data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody)';
EARTH_SC=x(:,1:6)-REF_EARTH*1e3;

% Convert position to ECEF (ITRF93) using the SPICE routine
for i=1:length(t)
    Rot = cspice_pxform( data.prop.Reference.Frame, 'ITRF93', t(i));
    r(i,1:3)=Rot*EARTH_SC(i,1:3)';
    v(i,1:3)=Rot*EARTH_SC(i,4:6)';
end

% Latitude and longitude
[lat_rad,lon_rad,h]=ecef2geodetic(r(:,1),r(:,2),r(:,3),referenceEllipsoid('wgs84'));
lat_deg=rad2deg(lat_rad);
lon_deg=rad2deg(lon_rad);

%Plot ground track
figure(1)
geoshow('landareas.shp', 'FaceColor', [0.5 1.0 0.5]);
axis tight
geoshow(lat_deg,lon_deg);


%Plot Altitude
figure(2)
plot((t-t0)/60,h/1e3)
xlabel('Time [min]')
ylabel('Altitude [km]')