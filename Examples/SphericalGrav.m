% SphericalGrav is a test script for OrbitProp
%
% This is script propagates an orbit using only an spherical gravity model.

%--- CODE ---%

%Clean up
clc
clear all
close all

%Add paths
addpath('..')
Add_AL_Dependencies();

%--- Initial state vector and model configuration ---%
%Earth parameters
Re = 6378136.49; %Equatorial Earth radius m [source: SMAD 3rd edition]
mu=398600.441e9;  %GM Earth m3/s2 [source: SMAD 3rd edition]

%Initial conditions
h = 200*1e3; %Initial altitude in m
i = 70; %Inclination
v0 = sqrt(mu/(Re+h)); %Initial velocity
x0 = [Re+h,0,0,0,v0*cosd(i),v0*sind(i)]; %Initial vector state
t0 = cspice_str2et( '2013 JAN 1 12:00:00' ); %Initial time of integration
tf = t0 + 10*90*60; %Final time of integration (180 minutes later)
dt = 60; % Integration solution output time step.

%Reference frame
data.prop.Reference.CentreBody='EARTH';
data.prop.Reference.Frame='J2000';

%Get x0 in Reference.Frame
Rot = cspice_pxform( data.prop.Reference.Frame, 'ITRF93', t0);
x0(1:3)=Rot\x0(1:3)';
x0(4:6)=Rot\x0(4:6)';

%Format initial conditions
data.prop.x0=x0;
data.prop.t=t0:dt:tf;

%Add waitbar
data.prop.waitbar=true;

%Models
data.models={@GravSpherical};

%--- Call function ---%
[data] = OrbitProp(data);

%--- Post process ---%
[figure_handle] = PlotGroundTrackEarth(data,'ITRF93');
