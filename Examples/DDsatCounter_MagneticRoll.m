% DDsatCo is a test script for OrbitProp
%
% In this case this example shows the orbit propagator in it's full
% potential using several models and simulating DDsat with the panels
% counter-rotating.
%
% In this case the roll is controlled through the magnetic torquers.
%
% The aerodynamic databases are in the AeroDB folder
%
% The propagation fails due to the magnetic torquers introducing unwanted
% torques.

%--- CODE ---%

%Clen up
clc
clear all
close all

%Add paths
addpath('..')
Add_AL_Dependencies();

%--- Initial state vector and model configuration ---%
%Earth parameters
Re = 6378136.49; %Equatorial Earth radius m [source: SMAD 3rd edition]
mu=398600.441e9;  %GM Earth m3/s2 [source: SMAD 3rd edition]
we = 7.292115e-5; %Earth Angular velocity in rad/s [source: SMAD 3rd edition]

%Initial conditions
h = 320*1e3; %Initial altitude in m
i = 79; %Inclination
v0 = sqrt(mu/(Re+h)); %Initial velocity
x0 = [Re+h,0,0,0,v0*cosd(i),v0*sind(i)]; %Initial vector state
t0 = cspice_str2et( '2010 JAN 1 12:00:00' ); %Initial time of integration
tf = t0+90*60; %Integrate until re-entry
dt = 1; %Integration time step
%Atmosphere co-rotation velocity
Vco = norm(cross([0;0;we],x0(1:3))); 

rx=deg2rad(-90);
ry=deg2rad(-90);
rz=deg2rad(-atand((v0*sind(i))/(v0*cosd(i)-Vco)));
q0=angle2quat(rx,ry,rz,'XYZ'); %Initial attitude quaternion
w0=[0,deg2rad(-360/90/60),0]; %Initial angular rate
%Format the initial 
x0=[x0,q0,w0];

%Reference frame
data.prop.Reference.CentreBody='EARTH';
data.prop.Reference.Frame='J2000';

%Get x0 in ITRF93
Rot = cspice_pxform( data.prop.Reference.Frame, 'ITRF93', t0);
x0(1:3)=Rot\x0(1:3)';
x0(4:6)=Rot\x0(4:6)';
%Change reference frame of the quaternions
R_q0=quat2dcm(q0);
R_q0 = R_q0*Rot;
x0(7:10)=dcm2quat(R_q0);

%Format initial conditions
data.prop.x0=x0;
data.prop.t=t0:dt:tf;

%Add waitbar
data.prop.waitbar=true;

%Spacecraft properties
data.prop.sc.I=[0.0049 ,0,0;
                0,0.0111,0;
                0,0,0.0111]; %Inertia
            
%Aerodynamic databases
rolldb_FP = 'AeroDB/Counter/pmz-45pmy0/F_roll_pmz-45_pmy0.mat';
pitchdb_FP = 'AeroDB/Counter/pmz-45pmy0/F_pitch_pmz-45_pmy0.mat';
yawdb_FP = 'AeroDB/Counter/pmz-45pmy0/F_yaw_pmz-45_pmy0.mat';
rolldb_FM = 'AeroDB/Counter/pmz45pmy0/F_roll_pmz45_pmy0.mat';
pitchdb_FM = 'AeroDB/Counter/pmz45pmy0/F_pitch_pmz45_pmy0.mat';
yawdb_FM = 'AeroDB/Counter/pmz45pmy0/F_yaw_pmz45_pmy0.mat';
rolldb_TP = 'AeroDB/Counter/pmz-45pmy0/T_roll_pmz-45_pmy0.mat';
pitchdb_TP = 'AeroDB/Counter/pmz-45pmy0/T_pitch_pmz-45_pmy0.mat';
yawdb_TP =  'AeroDB/Counter/pmz-45pmy0/T_yaw_pmz-45_pmy0.mat';
rolldb_TM = 'AeroDB/Counter/pmz45pmy0/T_roll_pmz45_pmy0.mat';
pitchdb_TM = 'AeroDB/Counter/pmz45pmy0/T_pitch_pmz45_pmy0.mat';
yawdb_TM = 'AeroDB/Counter/pmz45pmy0/T_yaw_pmz45_pmy0.mat';
            
%Models
data.models={@GravJ4};
data.models(end+1)={@AeroFDBCoWind};
data.models(end+1)={@SrpAccel6DOF};
data.models(end+1)={@AeroTDBCoWind};
data.models(end+1)={@MagFlowAttControl};
data.models(end+1)={@SrpTorque};
data.models(end+1)={@GravityGradient};

%Configure GravJ4 model
data.GravJ4.EarthFrame = 'ITRF93';

%Configure AeroFDBCoWind
FP_rolldb=load(rolldb_FP);
FP_pitchdb=load(pitchdb_FP);
FP_yawdb=load(yawdb_FP);
data.AeroFDBCoWind.rolldb=FP_rolldb.F_roll;
data.AeroFDBCoWind.pitchdb=FP_pitchdb.F_pitch;
data.AeroFDBCoWind.yawdb=FP_yawdb.F_yaw;
data.AeroFDBCoWind.atmos_model=@nrlmsise00;
data.AeroFDBCoWind.wind_model=@WindHWM93;
data.AeroFDBCoWind.mass=2;
data.AeroFDBCoWind.CfA0=[FP_rolldb.F_roll(1,2);FP_rolldb.F_roll(1,3);FP_rolldb.F_roll(1,4)];
data.AeroFDBCoWind.EarthFrame = 'ITRF93';

%Configure nrlmsise00
data.nrlmsise00.solar_activity = 'High'; %Level of solar activity
data.nrlmsise00.EarthFrame = 'ITRF93';

%Configure SrpAccel6DOF
SrpBCx= 2/(0.01*0.9);
SrpBCyz= 2/(0.1*0.9);
data.SrpAccel6DOF.invSrpBC = diag([1/SrpBCx,1/SrpBCyz,1/SrpBCyz]);
data.SrpAccel6DOF.EclipsingBody = 'EARTH';

%Configure AeroTDBCoWind model
TP_rolldb=load(rolldb_TP);
TP_pitchdb=load(pitchdb_TP);
TP_yawdb=load(yawdb_TP);
data.AeroTDBCoWind.rolldb = TP_rolldb.T_roll;
data.AeroTDBCoWind.pitchdb = TP_pitchdb.T_pitch;
data.AeroTDBCoWind.yawdb = TP_yawdb.T_yaw;
data.AeroTDBCoWind.atmos_model=@nrlmsise00;
data.AeroTDBCoWind.wind_model=@NoWind;
data.AeroTDBCoWind.CfA0l0=[TP_rolldb.T_roll(1,2);TP_rolldb.T_roll(1,3);TP_rolldb.T_roll(1,4)];
data.AeroTDBCoWind.EarthFrame = 'ITRF93';

%Configure WindHWM93
data.WindHWM93.solar_activity = 'Medium'; %Level of solar activity
data.WindHWM93.EarthFrame = 'ITRF93';

%Configure MagFlowAttControl
data.MagFlowAttControl.c = [2e-4;2e-4;2e-4];
data.MagFlowAttControl.w = [0;deg2rad(-360/90/60);0];
data.MagFlowAttControl.k = [1e-5;0;0];
data.MagFlowAttControl.att = [0;0;0];
data.MagFlowAttControl.A = [0.2;0.2;0.2];
data.MagFlowAttControl.PowerC = [2;1;1];
data.MagFlowAttControl.verb=1;
data.MagFlowAttControl.wind_model=@NoWind;
data.MagFlowAttControl.EarthFrame = 'ITRF93';

%Configure SrpTorque
data.SrpTorque.sun_vector = [1;0;0];
Alr= 0.1*0.9*0.1-0.01*0.9*0.1;
data.SrpTorque.Alr = [0,0,0;
                     0,0,Alr;
                     0,Alr,0];
data.SrpTorque.EclipsingBody = 'EARTH';

%--- Integrate ---%
[data] = OrbitProp(data);

%--- POST-PROCESS ---%
t=data.sol.t;
x=data.sol.x.(data.prop.Reference.CentreBody).(data.prop.Reference.Frame);

%--- Ground Track Plot ---%
PlotGroundTrackEarth(data,'ITRF93');


%--- Euler Angles plot ---%
%Get Euler angles
[r1,r2,r3] = quat2angle(x(:,7:10),'XYZ');
%Plot euler angles
figure
plot((t-t0)/60,rad2deg(r1),(t-t0)/60,rad2deg(r2),(t-t0)/60,rad2deg(r3));
xlabel('Time [min]')
ylabel('Euler angles [deg]')
legend('E1','E2','E3')
title('Euler angles')

%--- Rate plot ---%
figure
plot((t-t0)/60,rad2deg(x(:,11)),(t-t0)/60,rad2deg(x(:,12)),(t-t0)/60,rad2deg(x(:,13)));
xlabel('Time [min]')
ylabel('Angular rates [deg/s]')
legend('Roll','Pitch','Yaw')
title('Angular rates')

%--- Flow Attitude ---%

% Convert into EARTH coordinates for eclipse calculations
REF_EARTH=cspice_spkezr( 'EARTH', data.sol.t', data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody)';
EARTH_SC=data.sol.x.(data.prop.Reference.CentreBody).(data.prop.Reference.Frame)(:,1:6)-REF_EARTH*1e3;

roll=zeros(size(x,1),1); % Preallocate Variables
pitch=zeros(size(x,1),1); % Preallocate Variables
yaw=zeros(size(x,1),1); % Preallocate Variables
eclipse = zeros(size(x,1),1); % Preallocate Variables
for i=1:size(x,1)
    %Attitude with respect to the flow
    [roll(i),pitch(i),yaw(i)] = Attitude_Flow(t(i),x(i,:)',data,'AeroTDBCoWind');
    
    %Check for eclipse
    %Eclipsing body radius
    BODY_R = cspice_bodvrd( data.SrpAccel6DOF.EclipsingBody, 'RADII', 3 )*1e3;
    %Normalized Sun vector
    BODY_SUN=cspice_spkezr( 'SUN', t(i), data.prop.Reference.Frame,'NONE', data.SrpAccel6DOF.EclipsingBody );
    BODY_SUN=BODY_SUN(1:3)*1e3;
    eclipse(i) = check_eclipse(EARTH_SC(i,1:3)',BODY_SUN,BODY_R(1));
end

%--- Plot Flow Attitude ---%
figure
plot((t-t0)/60,rad2deg(roll),(t-t0)/60,rad2deg(pitch),(t-t0)/60,rad2deg(yaw),(t-t0)/60,eclipse,'k');
xlabel('Time [min]')
ylabel('Euler angles [deg]')
legend('Roll','Pitch','Yaw','Eclipse')
title('Attitude w.r.t the flow')


%--- Magnetic Plots ---%
%Calculate Torque
Torque = zeros(3,size(x,1));
Actuation_Level= zeros(3,size(x,1));
mfield_Body = zeros(3,size(x,1));
mfield_ECEI = zeros(3,size(x,1));
for i=1:length(t)
    %Magnetic Field
    [mfield_Body(1:3,i), mfield_ECEI(1:3,i)] = Magnetic_Field(t(i),x(i,:)',data,'MagFlowAttControl');
    %Torque target
    Torque_Target = -(x(i,11:13)'-data.MagFlowAttControl.w).*data.MagFlowAttControl.c + ... %Damping
                    -([roll(i);pitch(i);yaw(i)]-deg2rad(data.MagFlowAttControl.att)).*data.MagFlowAttControl.k; %Proportional
    %Actuation Level and Damping Torque
    [Torque(1:3,i),Actuation_Level(1:3,i)] = Magnetic_Torquer(t(i),x(i,:)',data,'MagFlowAttControl',mfield_Body(1:3,i),Torque_Target);
end

%---- Magnetic Torque Plot ---%
figure
plot((t-t0)/60,Torque(1,:),(t-t0)/60,Torque(2,:),(t-t0)/60,Torque(3,:))
xlabel('Time [mins]');
ylabel('Torque [Nm]');
legend('x Torquer','y Torquer','z Torquer');
title('Magnetic Damping Torque');

%--- Magnetic field ---%
figure
plot((t-t0)/60,mfield_Body(1,:),(t-t0)/60,mfield_Body(2,:),(t-t0)/60,mfield_Body(3,:))
xlabel('Time [mins]');
ylabel('Magnetic Field [T]');
legend('x Axis','y Axis','z Axis');
title('Magnetic Field in Body axis');

%--- Actuation Level ---%
figure
plot((t-t0)/60,Actuation_Level(1,:),(t-t0)/60,Actuation_Level(2,:),(t-t0)/60,Actuation_Level(3,:))
xlabel('Time [mins]');
ylabel('Actuation Level [Am2]');
legend('x Torquer','y Torquer','z Torquer');
title('Magnetic Torquers Actuation Level');

