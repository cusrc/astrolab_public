function [files] = Add_AL_Dependencies()
% Function that adds all necesarry functions to the path for AstroLab to
% work.
%
% The output files contains all the 
%
% This script is robust. It doesn't depends on the your current folder.
% This script should be executed before executing OrbitProp. If it not
% executed OrbitProp will fail displaying an error related to not finding
% the necessary functions.
%
% Warning! Errors will occurr if in the path to this file there are folders
% with the name 'Add_AL_Dependencies'.

%--- Copyright notice ---%
% Copyright 2012-2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab toolkit
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

%Get current full path
currentpath=strrep(mfilename('fullpath'),mfilename(),'');

%Add all the other folders to path
addpath(fullfile(currentpath,'Sys')); %Adds the System folder to the path
AddPathRecursive(fullfile(currentpath,'Models')); %Adds the Models to the path
AddPathRecursive(fullfile(currentpath,'Events')); %Adds the Events to the path
AddPathRecursive(fullfile(currentpath,'Utilities')); %Adds the Utilities to the path

% Add Spice Toolkit key folder
addpath(fullfile(currentpath,'mice/src/mice/')); %Adds SPICE toolkit
addpath(fullfile(currentpath,'mice/lib/')); %Adds SPICE toolkit
addpath(fullfile(currentpath,'mice/kernels/')); %Adds SPICE toolkit

%Spice Kernels
files{1} = which('naif0010.tls'); % Leap seconds Kernel.
files{2} = which('pck00010.tpc'); % This kernel contains orientation data for planets, natural satellites, the Sun, and selected asteroids.
files{3} = which('de430.bsp'); % The latest official JPL planetary ephemeris produced for general use.
files{4} = which('de-403-masses.tpc'); % Low accuracy, long term predict kernel of Earth orientation data.
files{5} = which('earth_070425_370426_predict.bpc'); % This Kernel gives "GM" (gravitational constant times mass) values for the sun, planets, and planetary system barycenters. These values are based on DE-403.

%Load Kernsels
for i=1:length(files)
    cspice_furnsh(files(i));
end

end


