% Validation_Spherical is a test script to Validate AstroLab under
% the EGM96 gravity model only.
%

%--- CODE ---%

%Clean up
clc
clear all
close all

%Add paths
addpath('../..')
Add_AL_Dependencies();

%--- Initial state vector and model configuration ---%
x=6878.137e3;	
y=0e3;
z=0e3;
vx=0e3;
vy=5.831596e3;
vz=4.89329e3;
x0 = [x,y,z,vx,vy,vz]; %Initial vector state
t0 = cspice_str2et( '2013 JUL 16 11:00:00' );
tf = cspice_str2et( '2013 JUL 17 11:00:00' );
dt = 60;

data.prop.Reference.CentreBody='EARTH';
data.prop.Reference.Frame='J2000';

%Format initial conditions
data.prop.x0=x0;
data.prop.t=t0:dt:tf;

%Models
data.models={@GravEGM96};

% Configure GravEGM96 model
data.GravEGM96.degree=21;
data.GravEGM96.EarthFrame = 'IAU_EARTH';

%--- Call function ---%
tic
[data] = OrbitProp(data);
toc

%--- Post process ---%
x=data.sol.x.(data.prop.Reference.CentreBody).(data.prop.Reference.Frame);
% Save results in a .csv file
dlmwrite('EGM96.csv', x(:,1:6)/1e3, 'delimiter', ',', 'precision', 9)
