% Validation_Spherical is a test script to Validate AstroLab under
% the EGM96 gravity model, the third body perturbations of the Sun and
% the Moon and a drag force.
%

%--- CODE ---%

%Clean up
clc
clear all
close all

%Add paths
addpath('../../..')
OrbitToPath

%--- Initial state vector and model configuration ---%
x=6878.137e3;	
y=0e3;
z=0e3;
vx=0e3;
vy=5.831596e3;
vz=4.89329e3;
x0 = [x,y,z,vx,vy,vz]; %Initial vector state
t0 = cspice_str2et( '2013 JUL 16 11:00:00' );
tf = cspice_str2et( '2013 JUL 17 11:00:00' );
dt = 60;

data.Reference.CentreBody='EARTH';
data.Reference.Frame='J2000';

%Models
data.models={@GravEGM96};
data.models(end+1)={@GravThirdBody};
data.models(end+1)={@ConstantBCCoWind};

% Configure GravEGM96 model
data.GravEGM96.degree=21;
data.GravEGM96.EarthFrame = 'IAU_EARTH';

% Configure GravThirdBody
data.GravThirdBody.Body={'SUN','MOON'};

%Configure ConstantBC model
data.ConstantBCCoWind.BC=1/0.02/2.2; %Ballisic coefficient
data.ConstantBCCoWind.atmos_model=@nrlmsise00;
data.ConstantBCCoWind.wind_model=@NoWind;
data.ConstantBCCoWind.EarthFrame = 'IAU_EARTH';

%Configure nrlmsise00
data.nrlmsise00.solar_activity = 'Medium'; %Level of solar activity
data.nrlmsise00.EarthFrame = 'IAU_EARTH';

%--- Call function ---%
tic
[t,x] = OrbitProp(x0,t0,[t0+dt:dt:tf],data);
toc

%--- Post process ---%
% Save results in a .csv file
dlmwrite('EGM96_SUN_MOON_DRAG.csv', x(:,1:6)/1e3, 'delimiter', ',', 'precision', 9)
