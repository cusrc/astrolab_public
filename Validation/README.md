# AstroLab Validation

Some of the AstroLab dynamic models have been validated comparing its results to [STK](http://www.agi.com/). 
The results of the validation can be seen in the `Results` folder. In addition the STK scenario used is provided in the `STK_Validation_Scenario` folder.
A description of the different scenarios validated is provided below:

## Spherical
In this case both STK and AstroLab are configured so that the Earth is modelled as a point mass.

## EGM96
In this case both STK and AstroLab are configured so that the Earth gravity is modelled with the EGM96 model. In both of them the gravity harmonics are truncated at the 21st degree. In the results spreadsheet is also compared the effect of the tides which can be configured in STK but not in AstroLab.

## EGM96_SUN_MOON
This case is the same as the EGM96 but it includes the third body perturbation of the Sun and the Moon.

## EGM96_SUN_MOON_SRP
This case is the same as the EGM96_SUN_MOON but it includes the effects of the Solar radiation pressure. In addition the start and stop times of the eclipse times are also compared.

## EGM96_SUN_MOON_DRAG
This case is the same as the EGM96_SUN_MOON but it includes the effects of the Drag.