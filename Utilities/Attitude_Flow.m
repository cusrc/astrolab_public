function [roll,pitch,yaw,flow,DCM_ECEI2Flow] = Attitude_Flow(t,x,data,model)
% This function computes the attitude of the spacecraft with respect to the
% flow.
%
% Inputs:
%   t -> Time in s.
%
%   x -> State vector.
%
%   data -> Data structure, contains the necessary information.
%
%   model -> Model name where to extract the specific data parameters.
%
% Paremetrs in data required:
%   data.model.wind_model -> Function handle specifying the wind
%   model. The output must be a 3x1 vector containing the wind in PCPF
%   coordinates.
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )
%
%   data.model.EarthFrame -> Earth reference frame to convert to ECEF.
%   It can be the 'IAU_EARTH' or the more precise 'ITRF93' frame.
%
% Outputs:
%   roll -> Roll rotation in rad.
%
%   pitch -> Pitch rotation in rad.
%
%   yaw -> Yaw rotation in rad.
%
%   flow -> Velocity of the relative flow in m/s.
%
%   DCM_ECEI2Flow -> DCM that converts from ECEI to Flow coordinates.

%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

%Flow velocity
[flow,~,~,DCM_ECEI2Flow] = Flow_Co_Wind(t,x,data,model);

%Rotation matrix from ECEI to Body ref
DCM_ECEI2Body=quat2dcm(x(7:10)');

%Rotation matrix from Flow to Body ref
DCM_Flow2Body = DCM_ECEI2Body*DCM_ECEI2Flow';

%Get angles
[roll, pitch, yaw] = dcm2angle(DCM_Flow2Body,'XYZ');

end