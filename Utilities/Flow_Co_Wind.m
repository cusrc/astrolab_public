function [Flow,Co_Rotation,Wind,DCM_ECEI2Flow] = Flow_Co_Wind(t,x,data,model)
% This function computes the relative flow velocity including the
% atmospheric co-rotation and the wind.
%
% Inputs:
%   t -> Time in s.
%
%   x -> State vector.
%
%   data -> Data structure, contains the necessary information.
%
%   model -> Model name where to extract the specific data parameters.
%
% Paremetrs in data required:
%   data.model.wind_model -> Function handle specifying the wind
%   model. The output must be a 3x1 vector containing the wind in PCPF
%   coordinates.
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )
%
%   data.model.EarthFrame -> Earth reference frame to convert to ECEF.
%   It can be the 'IAU_EARTH' or the more precise 'ITRF93' frame.
%
% Outputs:
%   Flow -> Velocity of the relative flow in m/s in data.prop.Reference.Frame.
%
%   Co_Rotation -> Atmospheric co-rotation in m/s in data.prop.Reference.Frame.
%
%   Wind -> Wind velocity in m/s in data.prop.Reference.Frame.
%
%   DCM_ECEI2Flow -> DCM that converts from ECEI to Flow coordinates.

%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

% Convert position and velocity to EARTH Reference frame
REF_EARTH=cspice_spkezr( 'EARTH', t, data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody);
EARTH_SC=x(1:6)-REF_EARTH(1:6)*1e3;

% Convert to ECEF
Rot = cspice_pxform( data.prop.Reference.Frame, data.(model).EarthFrame, t);
r=Rot*EARTH_SC(1:3);

%Flow is velocity - atmosphere rotation - wind
Wind = data.(model).wind_model(t,x,data); %Wind
we = 7.292115e-5; %Earth Angular velocity in rad/s [source: SMAD 3rd edition]
Co_Rotation = Rot'*cross([0;0;we],r); %Atmosphere co-rotation in m/s
Flow = EARTH_SC(4:6) - Co_Rotation - Wind; %Flow velocity in m/s

%Construct flow reference frame
xa = Flow/norm(Flow); %Velocity (roll axis)
zaux = -EARTH_SC(1:3)/norm(EARTH_SC(1:3)); %Nadir pointing
ya = cross(zaux,xa); % Velocity anti-parallel (pitch axis)
za = cross(xa,ya); % Near nadir (yaw axis)

%Rotation matrix from ECEI to Flow ref
DCM_ECEI2Flow=[xa';ya';za'];

end