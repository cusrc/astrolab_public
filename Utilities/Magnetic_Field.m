function [mfield_Body, mfield_ECEI] = Magnetic_Field(t,x,data,model)
% This function computes the attitude of the spacecraft with respect to the
% flow.
%
% Inputs:
%   t -> Time in s.
%
%   x -> State vector.
%
%   data -> Data structure, contains the necessary information.
%
%   model -> Model name where to extract the specific data parameters.
%
% Paremetrs in data required:
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )
%
%   data.model.EarthFrame -> Earth reference frame to convert to ECEF.
%   It can be the 'IAU_EARTH' or the more precise 'ITRF93' frame.
%
% Outputs:
%   mfield_Body -> Magnetic field in Teslas in Body ref frame.
%
%   mfield_ECEI -> Magnetic field in Teslas in ECEI ref frame.
%

%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

% Convert position and velocity to EARTH Reference frame
REF_EARTH=cspice_spkezr( 'EARTH', t, data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody);
EARTH_SC=x(1:3)-REF_EARTH(1:3)*1e3;

% Convert position to ECEF (ITRF93) using the SPICE routine
Rot = cspice_pxform( data.prop.Reference.Frame, data.(model).EarthFrame, t);
r=Rot*EARTH_SC;

% Latitude and longitude
[lat_rad,lon_rad,h]=ecef2geodetic(r(1),r(2),r(3),referenceEllipsoid('wgs84'));
lat_deg=rad2deg(lat_rad);
lon_deg=rad2deg(lon_rad);

%Magnetic field in T in ECEI
Year = str2double(cspice_timout( t, 'YYYY ::UTC'))+str2double(cspice_timout( t, 'DOY ::UTC'))/365;
if Year>2015 %IGRF11 limited to 2015
    Year=2015;
end
mfield_ECEI =Rot'*(dcmecef2ned(lat_deg,lon_deg)'*igrf11magm(h, lat_deg, lon_deg, Year)'/1e9);

%Change magnetic field to body axes.
DCM = quat2dcm(x(7:10)');
mfield_Body = DCM*mfield_ECEI;

end