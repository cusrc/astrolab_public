function [Torque_Provided,Actuation_Level] = Magnetic_Torquer(t,x,data,model,Magnetic_Field_Body,Torque_Target)
% This function computes the attitude of the spacecraft with respect to the
% flow.
%
% Inputs:
%   t -> Time in s.
%
%   x -> State vector.
%
%   data -> Data structure, contains the necessary information.
%
%   model -> Model name where to extract the specific data parameters.
%
%   Magnetic_Field_Body -> Magnetic field in Teslas in body axis.
%
%   Torque_Target -> Target torque in Nm.
%
% Paremetrs in data required:
%   data.(model).A -> Actuation level vector in Am2
%
%   data.(model).PowerC -> Torquers power vector in W/Am2
%
% Outputs:
%   Torque_Provided -> Torque provided by the torquers in Nm.
%
%   Actuation_Level -> Actuation level required to acheive the predicted 
%   torque in Am2.
%

%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

%--- Actuation Level (No saturation) ---%

%Preliminary actuation levels AL_p
AL_p=(cross(Magnetic_Field_Body,Torque_Target)/norm(Magnetic_Field_Body)^2);

%Scalar values corresponding to the limits
k_max=(data.(model).A-AL_p)./Magnetic_Field_Body; %Scalar value for maximum actuation level on one axis
k_min=(data.(model).A-AL_p)./Magnetic_Field_Body; %Scalar value for minus maximum actuation level on one axis
k_0=-AL_p./Magnetic_Field_Body; %Scalar value for 0 actuation level on one axis

%Go through the different possible actuation level (scalar values) to see if these are possible
AL=[];
for k=[k_max;k_min;k_0;0]'
    %Actuation level for the current scalar value
    AL_aux=AL_p+k*Magnetic_Field_Body;
    
    %Check if all the actuation values are below the actoator limits.
    if all(abs(AL_aux)<=data.(model).A)
        %Actiation level is possible(it doesn't go over the limits of the actuators)
        AL(1:3,end+1) = AL_aux; %Save actuation level
    end
        
end

%Check if there is any possible actuation levels
if size(AL,2)>0
    %There are possible actuation levels
    
    %Select the actuation level with minimum power consumption.
    [~,I] = min(sum(abs(AL.*(data.(model).PowerC*ones(1,size(AL,2)))))); 
    
    %Format output
    Actuation_Level = AL(1:3,I);
    Torque_Provided = cross(Actuation_Level,Magnetic_Field_Body);
    
    %Return
    return
end

%--- Actuation Level (Saturation) ---%

%Torque is not achievable, actuators saturate, get the nearest one.
if data.(model).verb; disp(['Magnetic torquers saturating (',model,').']); end;

%Scan through the different scalar values
T_pos=[];
T_diff_angle=[];
AL=[];
for k=[k_max;k_min;k_0]'
    %Actuation level for the current scalar value
    AL(1:3,end+1)=AL_p+k*Magnetic_Field_Body;
    
    if abs(AL(1,end))>data.(model).A(1)
        %Adjust to maximum
        AL(1,end)=sign(AL(1,end))*data.(model).A(1);
    end
    if abs(AL(2,end))>data.(model).A(2)
        %Adjust to maximum
        AL(2,end)=sign(AL(2,end))*data.(model).A(2);
    end
    if abs(AL(3,end))>data.(model).A(3)
        %Adjust to maximum
        AL(3,end)=sign(AL(3,end))*data.(model).A(3);
    end
    
    %Compute possible torque
    T_pos(1:3,end+1)=cross(AL(1:3,end),Magnetic_Field_Body);
    %Compute difference to desired torque in angle (deg)
    T_diff_angle(end+1)=acosd(dot(T_pos(1:3,end),Torque_Target)/norm(T_pos(1:3,end))/norm(Torque_Target));
    
end

%Get the actuation level with minimum angle difference
[~,I]=min(T_diff_angle);
Torque_Provided=T_pos(1:3,I);
Actuation_Level = AL(1:3,I);

end