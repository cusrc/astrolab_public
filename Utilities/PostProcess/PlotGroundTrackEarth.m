function [figure_handle] = PlotGroundTrackEarth(data,Earth_Frame)
% This function plots the ground track on Earth of the solution provided by
% AstroLab
%
% Inputs:
%   data -> Data structure, contains the necessary information.
%
%   Earth_Frame -> Earth reference frame to convert to ECEF.
%   It can be the 'IAU_EARTH' or the more precise 'ITRF93' frame.
%
% Outputs:
%   figure_handle -> Figure handle of the plor.
%

%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

% Convert into EARTH coordinates
REF_EARTH=cspice_spkezr( 'EARTH', data.sol.t', data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody)';
EARTH_SC=data.sol.x.(data.prop.Reference.CentreBody).(data.prop.Reference.Frame)(:,1:6)-REF_EARTH*1e3;

% Convert position to ECEF
for i=1:length(data.sol.t)
    Rot = cspice_pxform( data.prop.Reference.Frame, Earth_Frame, data.sol.t(i));
    r(i,1:3)=Rot*EARTH_SC(i,1:3)';
    v(i,1:3)=Rot*EARTH_SC(i,4:6)';
end

% Latitude and longitude
[lat_rad,lon_rad,h]=ecef2geodetic(r(:,1),r(:,2),r(:,3),referenceEllipsoid('wgs84'));
lat_deg=rad2deg(lat_rad);
lon_deg=rad2deg(lon_rad);

%Plot ground track
figure_handle = figure;
geoshow('landareas.shp', 'FaceColor', [0.5 1.0 0.5]);
axis tight
geoshow(lat_deg,lon_deg);

end