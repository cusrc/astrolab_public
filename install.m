% This script installs AstroLab. It should be executed before using
% AstroLab.
% 
% This script downloads the SPICE toolkit and the ephemeris files.

%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab toolkit
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%---- CODE ----%

%Clean and clear
clc;
clear all;

%Display
disp('Installing AstroLab . . .');

%Get current full path
currentpath=strrep(mfilename('fullpath'),mfilename(),'');

% Check operating system and architecture
C = computer;

% Download and uncompress the SPICE Toolkit into the mice folder.
fprintf('Downloading the SPICE Toolkit for %s.\n',C)
if strcmp(C,'PCWIN')
    % Microsoft Windows on x86
    
    % Download SPICE
    urlwrite('http://naif.jpl.nasa.gov/pub/naif/toolkit//MATLAB/PC_Windows_VisualC_MATLAB7.x_32bit/packages/mice.zip',fullfile(currentpath,'mice.zip'));
    disp('Done!')
    % Uncompress
    disp('Uncompressing the SPICE toolkit.')
    unzip(fullfile(currentpath,'mice.zip'),fullfile(currentpath,''));
    delete(fullfile(currentpath,'mice.zip'));
    disp('Done!')
    
elseif strcmp(C,'GLNX86')
    % Linux on x86
    
    % Download SPICE
    urlwrite('http://naif.jpl.nasa.gov/pub/naif/toolkit//MATLAB/PC_Linux_GCC_MATLAB7.x_32bit/packages/mice.tar.Z',fullfile(currentpath,'mice.tar.Z'));
    disp('Done!')
    % Uncompress
    disp('Uncompressing the SPICE toolkit.')
    cmnd=strcat({'uncompress '},{fullfile(currentpath,'mice.tar.Z')});
    unix(cmnd{1});
    untar(fullfile(currentpath,'mice.tar'),fullfile(currentpath,''));
    delete(fullfile(currentpath,'mice.tar'));
    disp('Done!')
    
elseif strcmp(C,'PCWIN64')
    %  Microsoft Windows on x64
    
    % Download SPICE
    urlwrite('http://naif.jpl.nasa.gov/pub/naif/toolkit//MATLAB/PC_Windows_VisualC_MATLAB7.x_64bit/packages/mice.zip',fullfile(currentpath,'mice.zip'));
    disp('Done!')
    % Uncompress
    disp('Uncompressing the SPICE toolkit.')
    unzip(fullfile(currentpath,'mice.zip'),fullfile(currentpath,''));
    delete(fullfile(currentpath,'mice.zip'));
    disp('Done!')
    
elseif strcmp(C,'GLNXA64')
    % Linux on x86_64
    
    % Download SPICE
    urlwrite('http://naif.jpl.nasa.gov/pub/naif/toolkit//MATLAB/PC_Linux_GCC_MATLAB7.x_64bit/packages/mice.tar.Z',fullfile(currentpath,'mice.tar.Z'));
    disp('Done!')
    % Uncompress
    disp('Uncompressing the SPICE toolkit.')
    cmnd=strcat({'uncompress '},{fullfile(currentpath,'mice.tar.Z')});
    unix(cmnd{1});
    untar(fullfile(currentpath,'mice.tar'),fullfile(currentpath,''));
    delete(fullfile(currentpath,'mice.tar'));
    disp('Done!')
    
elseif strcmp(C,'MACI64')
    % Apple Mac OS X on x86_6
    
    % Download SPICE
    urlwrite('http://naif.jpl.nasa.gov/pub/naif/toolkit//MATLAB/MacIntel_OSX_AppleC_MATLAB7.x_64bit/packages/mice.tar.Z',fullfile(currentpath,'mice.tar.Z'));
    disp('Done!')
    % Uncompress
    disp('Uncompressing the SPICE toolkit.')
    cmnd=strcat({'uncompress '},{fullfile(currentpath,'mice.tar.Z')});
    unix(cmnd{1});
    untar(fullfile(currentpath,'mice.tar'),fullfile(currentpath,''));
    delete(fullfile(currentpath,'mice.tar'));
    disp('Done!')
    
end

% Download the ephemeris files
disp('Downloading the ephemeris files . . .')
mkdir(fullfile(currentpath,'mice/kernels'));
urlwrite('http://naif.jpl.nasa.gov/pub/naif/generic_kernels/lsk/naif0010.tls',fullfile(currentpath,'mice/kernels/naif0010.tls')); % Leap seconds Kernel
urlwrite('http://naif.jpl.nasa.gov/pub/naif/generic_kernels/pck/pck00010.tpc',fullfile(currentpath,'mice/kernels/pck00010.tpc')); % This kernel contains orientation data for planets, natural satellites, the Sun, and selected asteroids.
urlwrite('http://naif.jpl.nasa.gov/pub/naif/generic_kernels/spk/planets/de430.bsp',fullfile(currentpath,'mice/kernels/de430.bsp')); % The latest official JPL planetary ephemeris produced for general use.
urlwrite('http://naif.jpl.nasa.gov/pub/naif/generic_kernels/pck/earth_070425_370426_predict.bpc',fullfile(currentpath,'mice/kernels/earth_070425_370426_predict.bpc')); % Low accuracy, long term predict kernel of Earth orientation data.
urlwrite('http://naif.jpl.nasa.gov/pub/naif/generic_kernels/pck/de-403-masses.tpc',fullfile(currentpath,'mice/kernels/de-403-masses.tpc')); % This Kernel gives "GM" (gravitational constant times mass) values for the sun, planets, and planetary system barycenters. These values are based on DE-403.

disp('Installation succesfull!')