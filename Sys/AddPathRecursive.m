function []=AddPathRecursive(path)
% This function adds folders and subfolders recursively to the path
%
% Parameters:
%   path -> Initial path to start adding the folders to the path.

%--- Copyright notice ---%
% Copyright 2012-2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab toolkit
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

% Add input folder to path.
addpath(path);

%Scan all items in this path.
D=dir(path);
for i=3:length(D) %Skip 1 and 2 as these are '.' and '..'
    %Check if this item is a folder
    if D(i).isdir
        %If it is a folder call the function again (to add them and 
        AddPathRecursive(fullfile(path,D(i).name))
    end
end

end