function [A,T,E] = MagBdotError(t,x,data)
% MagBdotError is a model that uses magnetorquers to damp undesired initial
% rotation rate after spacecraft are deployed from launcher. The actuation
% level required to drive magnetorquer is calculated from changing of
% magnetic field (Bdot) which is roughly calculated by product of angular
% rate and current magnetic field.  This model includes all sources of
% bias/error/uncertainties from actuator and sensors.

% The Earth magnetic field is modelled using the IGRF-11 and therefore the
% model requires the igrf11magm MATLAB function.
%
% Parameters:
%   data.MagBdoErrort.A -> Actuation level in Am2
%
%   data.MagBdotError.C -> Control factor (scalar)
%
%   data.MagBdotError.PowerC -> Torquers power vector in W/Am2
%
%   data.MagBdotError.MTB -> Actuation level bias vector in Am2
%
%   data.MagBdotError.MMR -> Magnetometre resolution in Teslas
%
%   data.MagBdotError.MTR -> Magnetorquer resolution in Am2
%
%   data.MagBdotError.verb -> True for verbose mode
%   
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )
%
%   data.MagBdotError.EarthFrame -> Earth reference frame to convert to ECEF.
%   It can be the 'IAU_EARTH' or the more precise 'ITRF93' frame.

%--- Copyright notice ---%
% Copyright 2012-2013 Cranfield University
% Written by Josep Virgili and Daniel Zhou Hao
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%
%--- CODE ---%

%--- Compute Magnetic Field ---%
[Mag_Field_Body_True] = Magnetic_Field(t,x,data,'MagBdotError');

%Add error to the true magnetic field to simulate that it is the measured
Mag_Field_Body_Meas = round(Mag_Field_Body_True./data.MagBdotError.MMR).*data.MagBdotError.MMR;

%--- Traget torque ---%

%Compute Bdot
Bdot = cross(Mag_Field_Body_Meas,(x(11:13)));

%Compute target actuation level
AL_Target = -data.MagBdot.C * Bdot;

%Compute target torque
Torque_Target = cross(AL_Target,Mag_Field_Body);

%--- Compute torque ---%
[~,Actuation_Level_Target] = Magnetic_Torquer(t,x,data,'MagBdotError',Mag_Field_Body,Torque_Target);

%Add error to the target actuation level to simulate the one achieved (includes resolution and bias).
Actuation_level_Achieved = round(Actuation_Level_Target./data.MagBdotError.MMR).*data.MagBdotError.MMR+data.MagBdotError.MTB;

%Compute the torque
T=cross(Actuation_level_Achieved,Mag_Field_Body_True);

%Acceleration (this model don't produce linear acceleration)
A=[0;0;0];
%Extra state variables (this model doesn't need extra state variables)
E=zeros(1,length(x)-13);

end


