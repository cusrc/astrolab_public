function [A,T,E] = MagBdot(t,x,data)
% BdotDamping is a model that uses magnetorquers to damp undesired initial
% rotation rate after spacecraft are deployed from launcher. The actuation
% level required to drive magnetorquer is calculated from changing of
% magnetic field (Bdot) which is roughly calculated by product of angular
% rate and current magnetic field. 

% The Earth magnetic field is modelled using the IGRF-11 and therefore the
% model requires the igrf11magm MATLAB function.
%
% Parameters:
%   data.MagBdot.A -> Magnetorquer actuation level in Am2
%
%   data.MagBdot.C -> Control factor (scalar)
%
%   data.MagBdot.PowerC -> Torquers power vector in W/Am2
%
%   data.MagBdot.verb -> True for verbose mode
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )
%
%   data.MagBdot.EarthFrame -> Earth reference frame to convert to ECEF.
%   It can be the 'IAU_EARTH' or the more precise 'ITRF93' frame.


%--- Copyright notice ---%
% Copyright 2012-2013 Cranfield University
% Written by Josep Virgili and Daniel Zhou Hao
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

%--- Compute Magnetic Field ---%
[Mag_Field_Body] = Magnetic_Field(t,x,data,'MagBdot');

%--- Traget torque ---%

%Compute Bdot
Bdot = cross(Mag_Field_Body,(x(11:13)));

%Compute target actuation level
AL_Target = -data.MagBdot.C * Bdot;

%Compute target torque
Torque_Target = cross(AL_Target,Mag_Field_Body);

%--- Compute torque ---%
[T] = Magnetic_Torquer(t,x,data,'MagBdot',Mag_Field_Body,Torque_Target);

%Acceleration (this model don't produce linear acceleration)
A=[0;0;0];
%Extra state variables (this model doesn't need extra state variables)
E=zeros(1,length(x)-13);

end


