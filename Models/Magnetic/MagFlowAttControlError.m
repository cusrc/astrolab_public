function [A,T,E,Actuation_level_Achieved,Mag_Field_Body_Meas] = MagFlowAttControlError(t,x,data)
% MagFlowAttControlError is a model that produced control torques through 
% magnetic torquers that react to attitude rates and the attiude with 
% respect to the flow. The magnetic torquers are modelled as real.
%
% The Earth magnetic field is modelled using the IGRF-11 and therefore the
% model requires the igrf11magm MATLAB function.
%
% Parameters:
%   data.MagFlowAttControlError.c -> Torque per rad/s. Damping ratio (3 x 1) vector. T = - c * w
%
%   data.MagFlowAttControlError.w -> Body rate bias rad/s
%
%   data.MagFlowAttControlError.k -> Proportional constant. T =  - roll * k
%
%   data.MagFlowAttControlError.att -> Attitude bias in deg att = [roll, pitch and yaw]
%
%   data.MagFlowAttControlError.A -> Actuation level vector in Am2
%
%   data.MagFlowAttControlError.PowerC -> Torquers power vector in W/Am2
%
%   data.MagFlowAttControlError.MTB -> Actuation level bias vector in Am2
%
%   data.MagFlowAttControlError.MMR -> Magnetometer resolution in Teslas
%
%   data.MagFlowAttControlError.MTR -> Magnetorquer resolution in Am2 
%
%   data.MagFlowAttControlError.GyroErrorDB -> Gyroscope error DB in rad/s. It is 
%   a nx2 matric with the first column being the time vector and the second
%   being the error in the gyro.
%
%   data.MagFlowAttControlError.verb -> True for verbose mode
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )
%
%   data.MagFlowAttControlError.EarthFrame -> Earth reference frame to convert to ECEF.
%   It can be the 'IAU_EARTH' or the more precise 'ITRF93' frame.
%
% Outputs:
%   Actuation_Level_Achieved -> Actuation level achieved to the magnetic 
%   torquers in Am2.
%
%   Mag_Field_Body_Meas -> Magnetic field measures in Teslas in Body ref 
%   frame.
%

%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

%--- Attitude in flow reference frame ---%
[r1,r2,r3] = Attitude_Flow(t,x,data,'MagFlowAttControlError');

%Add error to the angular rates
angular_rates = x(11:13) + interp1(data.MagFlowAttControlError.GyroErrorDB(:,1),data.MagFlowAttControlError.GyroErrorDB(:,2),t);

%Check if data.MagDampingError.GyroErrorDB contains data for the present time step
if isnan(angular_rates)
    error('Error: The data.MagDampingError.GyroErrorDB does not contain data for the current time step.');
end

%--- Traget torque ---%
Torque_Target = -(angular_rates-data.MagFlowAttControlError.w).*data.MagFlowAttControlError.c + ... %Damping
                -([r1;r2;r3]-deg2rad(data.MagFlowAttControlError.att)).*data.MagFlowAttControlError.k; %Proportional

%--- Compute Magnetic Field ---%
[Mag_Field_Body_True] = Magnetic_Field(t,x,data,'MagFlowAttControlError');

%Add error to the true magnetic field to simulate that it is the measured
Mag_Field_Body_Meas = round(Mag_Field_Body_True./data.MagFlowAttControlError.MMR).*data.MagFlowAttControlError.MMR;

%--- Compute torque ---%
[~,Actuation_Level_Target] = Magnetic_Torquer(t,x,data,'MagFlowAttControlError',Mag_Field_Body_Meas,Torque_Target);

%Add error to the target actuation level to simulate the one achieved (includes resolution and bias).
Actuation_level_Achieved = round(Actuation_Level_Target./data.MagFlowAttControlError.MMR).*data.MagFlowAttControlError.MMR+data.MagFlowAttControlError.MTB;

%Compute the torque
T=cross(Actuation_level_Achieved,Mag_Field_Body_True);

%Acceleration (this model don't produce linear acceleration)
A=[0;0;0];

%Extra state variables (this model doesn't need extra state variables)
E=zeros(1,length(x)-13);

end