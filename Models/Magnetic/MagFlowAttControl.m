function [A,T,E,Actuation_Level,Mag_Field_Body] = MagFlowAttControl(t,x,data)
% MagFlowAttControl is a model that produced control torques through 
% magnetic torquers that react to attitude rates and the attiude with 
% respect to the flow. The magnetic torquers are modelled as ideal.
%
% The Earth magnetic field is modelled using the IGRF-11 and therefore the
% model requires the igrf11magm MATLAB function.
%
% Parameters:
%   data.MagFlowAttControl.c -> Torque per rad/s. Damping ratio (3 x 1) vector. T = - c * w
%
%   data.MagFlowAttControl.w -> Body rate bias rad/s
%
%   data.MagFlowAttControl.k -> Proportional constant. T =  - roll * k
%
%   data.MagFlowAttControl.att -> Attitude bias in deg att = [roll, pitch and yaw]
%
%   data.MagFlowAttControl.A -> Actuation level vector in Am2
%
%   data.MagFlowAttControl.PowerC -> Torquers power vector in W/Am2
%
%   data.MagFlowAttControl.verb -> True for verbose mode
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )
%
%   data.MagFlowAttControl.EarthFrame -> Earth reference frame to convert to ECEF.
%   It can be the 'IAU_EARTH' or the more precise 'ITRF93' frame.
%
% Outputs:
%   Actuation_Level -> Actuation level required to acheive the predicted 
%   torque in Am2.
%
%   Mag_Field_Body -> Magnetic field in Teslas in Body ref frame.
%

%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

%--- Attitude in flow reference frame ---%
[r1,r2,r3] = Attitude_Flow(t,x,data,'MagFlowAttControl');

%--- Traget torque ---%
Torque_Target = -(x(11:13)-data.MagFlowAttControl.w).*data.MagFlowAttControl.c + ... %Damping
                -([r1;r2;r3]-deg2rad(data.MagFlowAttControl.att)).*data.MagFlowAttControl.k; %Proportional

%--- Compute Magnetic Field ---%
[Mag_Field_Body] = Magnetic_Field(t,x,data,'MagFlowAttControl');

%--- Compute torque ---%
[T,Actuation_Level] = Magnetic_Torquer(t,x,data,'MagFlowAttControl',Mag_Field_Body,Torque_Target);

%Acceleration (this model don't produce linear acceleration)
A=[0;0;0];

%Extra state variables (this model doesn't need extra state variables)
E=zeros(1,length(x)-13);

end