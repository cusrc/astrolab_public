function [A,T,E] = Damping(t,x,data)
% Damping is a model that damps the unwanted angular rates by using 
% ideal dampers.
%
% Parameters:
%   data.Damping.c -> Torque per rad/s. Damping ratio

%--- Copyright notice ---%
% Copyright 2012-2013 Cranfield University
% Written by Gemma Saura Carretero and Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

%--- Desired torque ---%
T = -(x(11:13)-data.Damping.w).*data.Damping.c;

%Acceleration (this model don't produce linear acceleration)
A=[0;0;0];

%Extra state variables (this model doesn't need extra state variables)
E=zeros(1,length(x)-13);

end