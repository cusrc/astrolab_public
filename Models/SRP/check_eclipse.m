function [eclipse] = check_eclipse(sc,su,R)
% The function check_eclipse check if spacecraft is under eclipse. Assumes
% a spherical Earth.
% 
% Inputs:
%   sc -> The spacecraft vector from the eclipsing body.
%   su -> The Sun vector from the eclipsing body.
%   R -> The eclipsing body radius in m.
%
% Outputs:
%   eclipse -> True if the spacecraft is in eclipse.
%

%--- Copyright notice ---%
% Copyright 2012-2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

%Coordinate transformation
x=(su-sc)/norm(su-sc);
z=cross(su,sc);
z=z/norm(z);
y=cross(z,x);

%Get the rotation matrix
DCM = [x,y,z]';

%Transform the s/c vector to the new system
sc_p = DCM*sc;

%Check if s/c is in eclipse
if sc_p(2)<R && sc_p(1)<0
    %S/C is in full eclipse
    eclipse=true;
    return
else
    eclipse=false;
    return
end

end