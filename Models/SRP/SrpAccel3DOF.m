function [A,T,E] = SrpAccel3DOF(t,x,data)
% SrpAccel3DOF is a model to compute the accelerations produced by the solar
% radiation pressure (SRP). This forces are dependant on the spacecraft
% attitude w.r.t the sun. To determine this forces the SRP ballistic 
% coefficients (m/r/A) are passed in a 3x3 matrix.
%
% Parameters:
%   data.SrpAccel3DOF.invSrpBC -> The inverse SRP ballistic coefficient 
%   r*A/m.
%
%   data.SrpAccel3DOF.EclipsingBody -> Eclipsing body (Earth, Jupiter, ... ) 
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )


%--- Copyright notice ---%
% Copyright 2012-2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%
% Convert position and velocity to EARTH Reference frame
REF_BODY=cspice_spkezr( data.SrpAccel3DOF.EclipsingBody, t, data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody);
BODY_SC=x(1:3)-REF_BODY(1:3)*1e3;

%Normalized Sun vector
BODY_SUN=cspice_spkezr( 'SUN', t, data.prop.Reference.Frame,'NONE', data.SrpAccel3DOF.EclipsingBody );
BODY_SUN=BODY_SUN(1:3)*1e3;

%Eclipsing body radius
BODY_R = cspice_bodvrd( data.SrpAccel3DOF.EclipsingBody, 'RADII', 3 )*1e3;

%Check if spacecraft is in eclipse
if check_eclipse(BODY_SC,BODY_SUN,BODY_R(1))
    %Spacecraft in eclipse
    A = [0;0;0];
else
    %Spacecraft is not in eclipse
    Sun_P = 4.575e-6*(norm(BODY_SUN-BODY_SC)/149597870700)^2; %Sun pressure in N/m2
    %Compute Acceleration
    A=data.SrpAccel3DOF.invSrpBC*Sun_P;
    
    %Get the correct sign and illumination fractions
    A=A*BODY_SUN/norm(BODY_SUN);
    
end

%Torque (this model don't produce torque)
T=[0;0;0];

%Extra state variables (this model doesn't need extra state variables)
E=zeros(1,length(x)-13);

end