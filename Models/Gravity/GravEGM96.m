function [A,T,E] = GravEGM96(t,x,data)
% Implements the EGM96 gravity model.
%
% Requires the gravitysphericalharmonic MATLAB function.
%
% Parameters:
%   data.GravEGM96.degree -> Gravity harmonics degree (optional, by default
%   is 70, max 360).
%
%   data.prop.Reference.Frame -> Reference frame (J2000).
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.GravEGM96.EarthFrame -> Earth reference frame to convert to ECEF.
%   It can be the 'IAU_EARTH' or the more precise 'ITRF93' frame.

%--- Copyright notice ---%
% Copyright 2012-2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

% Convert position and velocity to EARTH Reference frame
REF_EARTH=cspice_spkezr( 'EARTH', t, data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody);
EARTH_SC=x(1:3)-REF_EARTH(1:3)*1e3;

% Convert position to ECEF (data.GravEGM96.EarthFrame) using the SPICE routine
Rot = cspice_pxform( data.prop.Reference.Frame, data.GravEGM96.EarthFrame, t);
r=Rot*EARTH_SC;

%Compute the gravity acceleration.
if isfield(data.GravEGM96,'degree')
    [a(1,1), a(2,1), a(3,1)] = gravitysphericalharmonic( r', 'EGM96', data.GravEGM96.degree);
else
    [a(1,1), a(2,1), a(3,1)] = gravitysphericalharmonic( r', 'EGM96');
end


%Get acceleration back to original coordinates
A = Rot\a;

%Torque (this model doesn't create any torque)
T=[0,0,0]';

%Extra state variables (this model doesn't need extra state variables)
E=zeros(1,length(x)-13);

end