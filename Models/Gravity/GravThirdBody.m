function [A,T,E] = GravThirdBody(t,x,data)
% Implements a third body gravity perturbation. Third Bodies are modelled
% as point masses.
% 
% Parameters:
%   data.GravThirdBody.Body -> Cell string vector with the names of the
%   third bodies.
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )
%

%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

% Initialize acceleration vector
A=[0;0;0];

%Scan through the bodies
for i=1:length(data.GravThirdBody.Body)
    
    %Position of the planet
    REF_BODY=cspice_spkezr( data.GravThirdBody.Body(i), t, data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody);
    REF_BODY=REF_BODY(1:3)*1e3;
    BODY_SC=x(1:3)-REF_BODY;
    
    %GM of the planet
    BODY_GM=cspice_bodvrd( data.GravThirdBody.Body(i), 'GM', 1 )*1e9;
    
    %Third body acceleration
    A=A+BODY_GM*( -BODY_SC/norm(BODY_SC)^3 - REF_BODY/norm(REF_BODY)^3 );
    
end

%Torque (this model doesn't create any torque)
T=[0,0,0]';

%Extra state variables (this model doesn't need extra state variables)
E=zeros(1,length(x)-13);
