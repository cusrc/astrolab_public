function [A,T,E] = GravityGradient(t,x,data)
% GravityGradient is a model to compute the torques produced by the gravity
% gradient. This torques are dependant on the spacecraft attitude with
% respect to the Earth.
%
% Parameters:
%   data.prop.sc.I -> Spacecarft inertia matrix [3x3] in kg*m^2.
%
%   data.prop.Reference.Frame -> Reference frame (J2000).
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).

%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

% Convert position and velocity to EARTH Reference frame
REF_EARTH=cspice_spkezr( 'EARTH', t, data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody);
EARTH_SC=x(1:3)-REF_EARTH(1:3)*1e3;

%Parameters
mu=398600.441e9;  %GM Earth m3/s2 [source: SMAD 3rd edition]

% Get position vector in body axes
DCM = quat2dcm(x(7:10)');
R = DCM*EARTH_SC;

%Compute torque in body axes
T=3*mu/norm(R)^3*cross(R/norm(R),data.prop.sc.I*(R/norm(R)));

%Acceleration (this model don't produce linear acceleration)
A=[0;0;0];

%Extra state variables (this model doesn't need extra state variables)
E=zeros(1,length(x)-13);
end