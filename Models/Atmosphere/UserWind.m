function [w] = UserWind(t,x,data)
% UserWind model outputs the user-input wind value.
%
% This model should be used when the wind value must be manually set.
%
% Parameters:
%   data.UserWind.w input scalar or vector for the wind value.

%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by David de la Torre and Josep Virgili
%
% This file is part of the AstroLab toolkit
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%---- CODE ----%

% Returns the input value for wind
w = data.UserWind.w;

end