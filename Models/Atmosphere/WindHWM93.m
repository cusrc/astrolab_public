function [w] = WindHWM93(t,x,data)
% WindHWM93 implements the HWM93 wind models and gives the output in a 3x1
% wind vector in ECEI coordinates
%
% WARNING! This model needs compilation of Fortran code.
% Please make sure that the compiled version for your platform exists. If 
% not refer to the source code and compile.
%
% Parameters:
%   data.WindHWM93.solar_activity to select different solar activity 
%   scenarios (options: 'Low', 'Medium', 'High', 'HighShort' or 'Custom'. 
%   If 'Custom' is selected then data.WindHWM93.f107Average,
%   data.WindHWM93.f107Daily, data.WindHWM93.ap_daily and the
%   data.WindHWM93.ap_3hour must be specified).
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )
%
%   data.WindHWM93.EarthFrame -> Earth reference frame to convert to ECEF.
%   It can be the 'IAU_EARTH' or the more precise 'ITRF93' frame.


%--- Copyright notice ---%
% Copyright 2012-2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab toolkit
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%---- CODE ----%

%--- Prepare input data --%
%Date and time
%Date and time
dayOfYear = str2double(cspice_timout( t, 'DOY ::UTC')); %Day of the year
UTseconds = str2double(cspice_timout( t, 'HR ::UTC'))*3600+...
    str2double(cspice_timout( t, 'MN ::UTC'))*60+...
    str2double(cspice_timout( t, 'SC.## ::UTC')); %UT seconds

% Convert position and velocity to EARTH Reference frame
REF_EARTH=cspice_spkezr( 'EARTH', t, data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody);
EARTH_SC=x(1:3)-REF_EARTH(1:3)*1e3;

% Convert position to ECEF (ITRF93) using the SPICE routine
Rot = cspice_pxform( data.prop.Reference.Frame, data.WindHWM93.EarthFrame, t);
r=Rot*EARTH_SC;

% Latitude and longitude and altitude
[lat_rad,lon_rad,h]=ecef2geodetic(r(1),r(2),r(3),referenceEllipsoid('wgs84'));
lat_deg=rad2deg(lat_rad);
lon_deg=rad2deg(lon_rad);
h = h/1e3; %Altitude in km

% Apparent solar time
[s_hr, s_min, s_sec]=cspice_et2lst( t, 399, lon_rad, 'PLANETOCENTRIC');
localApparentSolarTime=s_hr+s_min/60+s_sec/3600;

%Solar activity
if strcmp(data.WindHWM93.solar_activity,'Custom') %TODO allow solar input as function.
    f107a = data.WindHWM93.f107Average;
    f107 = data.WindHWM93.f107Daily;
    ap_daily = data.WindHWM93.ap_daily;
    ap_3hour = data.WindHWM93.ap_3hour;
elseif strcmp(data.nrlmsise00.solar_activity,'Low')
    f107a = 65;
    f107 = 65;
    ap_daily = 0;
    ap_3hour = 0;
elseif strcmp(data.nrlmsise00.solar_activity,'Medium')
    f107a = 140;
    f107 = 140;
    ap_daily = [15,15,15,15,15,15,15];
    ap_3hour = 15;
elseif strcmp(data.nrlmsise00.solar_activity,'High')
    f107a = 250;
    f107 = 250;
    ap_daily = 45;
    ap_3hour = 45;
elseif strcmp(data.nrlmsise00.solar_activity,'HighShort')
    f107a = 250;
    f107 = 300;
    ap_daily = 240;
    ap_3hour = 240;
else 
    error('Error: Incorrect solar activity option')
end

%--- Compute wind ---%
wind = hwm93_raw(dayOfYear,UTseconds,h,lat_deg,lon_deg,localApparentSolarTime,f107a,f107,ap_daily,ap_3hour); 

%--- Convert to ECEI frame ---%
w = Rot' * [-wind(1)*sind(lat_deg)*cosd(lon_deg)-wind(2)*sind(lon_deg);...
            -wind(1)*sind(lat_deg)*sind(lon_deg)+wind(2)*cosd(lon_deg);...
            +wind(1)*cosd(lat_deg)];


end