function [out] = nrlmsise00(t,x,data)
% nrlmsise00 implements the NRLMSISE-00 atmospheric model. The output is
% compliant with the standard output.
% 
% Parameters:
%   data.nrlmsise00.solar_activity to select different solar activity 
%   scenarios (options: 'Low', 'Medium', 'High', 'HighShort' or 'Custom'. 
%   If 'Custom' is selected then data.nrlmsise00.f107Average,
%   data.nrlmsise00.f107Daily and data.magneticIndex must be specified)
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )
%
%   data.nrlmsise00.EarthFrame -> Earth reference frame to convert to ECEF.
%   It can be the 'IAU_EARTH' or the more precise 'ITRF93' frame.
%
% Output:
%   out.Texo-> Exospheric temperature in K.
%   out.Talt-> Altitude temperature in K.
%   out.rhoHe-> Helium particles per m3.
%   out.rhoO-> Atomic oxygen particles per m3.
%   out.rhoN2-> Diatomic nitrogen particles per m3.
%   out.rhoO2-> Diatomic oxygen particles per m3.
%   out.rhoAr-> Argon particles per m3.
%   out.rho-> HeTotal density in kg/m3.
%   out.rhoH-> Hydrogen particles per m3.
%   out.rhoN-> Atmoic Nitrogen particles per m3.
%   out.rhoOa-> Anomalous oxygen particles per m3.


%--- Copyright notice ---%
% Copyright 2012-2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab toolkit
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%---- CODE ----%

%--- Prepare input data --%
%Date and time
dayOfYear = str2double(cspice_timout( t, 'DOY ::UTC')); %Day of the year
UTseconds = str2double(cspice_timout( t, 'HR ::UTC'))*3600+...
    str2double(cspice_timout( t, 'MN ::UTC'))*60+...
    str2double(cspice_timout( t, 'SC.## ::UTC')); %UT seconds

%Solar activity
if strcmp(data.nrlmsise00.solar_activity,'Custom') %TODO allow solar input as function.
    f107Average = data.nrlmsise00.f107Average;
    f107Daily = data.nrlmsise00.f107Daily;
    magneticIndex = data.nrlmsise00.magneticIndex;
elseif strcmp(data.nrlmsise00.solar_activity,'Low')
    f107Average = 65;
    f107Daily = 65;
    magneticIndex = [0,0,0,0,0,0,0];
elseif strcmp(data.nrlmsise00.solar_activity,'Medium')
    f107Average = 140;
    f107Daily = 140;
    magneticIndex = [15,15,15,15,15,15,15];
elseif strcmp(data.nrlmsise00.solar_activity,'High')
    f107Average = 250;
    f107Daily = 250;
    magneticIndex = [45,45,45,45,45,45,45];
elseif strcmp(data.nrlmsise00.solar_activity,'HighShort')
    f107Average = 250;
    f107Daily = 300;
    magneticIndex = [240,240,240,240,240,240,240];
else 
    error('Error: Incorrect solar activity option')
end

% Convert position and velocity to EARTH Reference frame
REF_EARTH=cspice_spkezr( 'EARTH', t, data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody);
EARTH_SC=x(1:3)-REF_EARTH(1:3)*1e3;

% Convert position to ECEF (ITRF93) using the SPICE routine
Rot = cspice_pxform( data.prop.Reference.Frame, data.nrlmsise00.EarthFrame, t);
r=Rot*EARTH_SC;

% Latitude and longitude
[lat_rad,lon_rad,h]=ecef2geodetic(r(1),r(2),r(3),referenceEllipsoid('wgs84'));
lat_deg=rad2deg(lat_rad);
lon_deg=rad2deg(lon_rad);

% Apparent solar time
[s_hr, s_min, s_sec]=cspice_et2lst( t, 399, lon_rad, 'PLANETOCENTRIC');
localApparentSolarTime=s_hr+s_min/60+s_sec/3600;

%--- Compute the atmospheric properties ---%
[T, rho] = atmosnrlmsise00(h, lat_deg, lon_deg, 0, dayOfYear, UTseconds, localApparentSolarTime,f107Average, f107Daily, magneticIndex,'Oxygen','Warning');
    
%--- Format density and temperature data ---%
out.Texo=T(:,1);
out.Talt=T(:,2);
out.rhoHe=rho(:,1);
out.rhoO=rho(:,2);
out.rhoN2=rho(:,3);
out.rhoO2=rho(:,4);
out.rhoAr=rho(:,5);
out.rho=rho(:,6);
out.rhoH=rho(:,7);
out.rhoN=rho(:,8);
out.rhoOa=rho(:,9);

end