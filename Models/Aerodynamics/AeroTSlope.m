function [A,T,E] = AeroTSlope(t,x,data)
% AeroTSlope is a model to compute the aerodynamic torques that act on
% the spacecraft. This torues are proportional to the angle with respect to 
% the flow plus an offset T = q*k*theta+q*CT0, where q is the dynamic 
% pressure (hence the slope name).
% This model assumes that the atmosphere co-rotates with the Earth and 
% allows for the possibility to include wind.
%
% Parameters:
%   data.AeroTSlope.k_Roll -> Slope of T = q*k_Roll * Roll + q*CT0_Roll in Nm/rad;
%
%   data.AeroTSlope.k_Pitch -> Slope of T = q*k_Pitch * Pitch + q*CT0_Pitch in Nm/rad;
%
%   data.AeroTSlope.k_Yaw -> Slope of T = q*k_Yaw * Yaw + qCT0_Yaw in Nm/rad;
%
%   data.AeroTSlope.CT0_Roll -> Offset torque of T = q*k_Roll * Roll + q*CT0_Roll in Nm/rad;
%
%   data.AeroTSlope.CT0_Pitch -> Offset torque of T = q*k_Pitch * Pitch + q*CT0_Pitch in Nm/rad;
%
%   data.AeroTSlope.CT0_Yaw -> Offset torque of T = q*k_Yaw * Yaw + q*CT0_Yaw in Nm/rad;
% 
%   data.AeroTSlope.atmos_model -> Function handle specifying the
%   atmospheric model. The output of the atmospheric model must comply with
%   the standard format and at least contain the atmos.rho field specifying
%   the atmospheric density in kg/m3.
%
%   data.AeroTSlope.wind_model -> Function handle specifying the wind
%   model. The output must be a 3x1 vector containg the wind in PCPF
%   coordinates.
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )
%
%   data.AeroTSlope.EarthFrame -> Earth reference frame to convert to ECEF.
%   It can be the 'IAU_EARTH' or the more precise 'ITRF93' frame.


%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

%--- Attitude in flow reference frame ---%
[r1,r2,r3,flow] = Attitude_Flow(t,x,data,' AeroTSlope');

%Atmospheric properties
atmos = data.AeroTSlope.atmos_model(t,x,data);

%Final torque
k =  [data.AeroTSlope.k_Roll;
      data.AeroTSlope.k_Pitch;
      data.AeroTSlope.k_Yaw];
CT0 = [data.AeroTSlope.CT0_Roll;
      data.AeroTSlope.CT0_Pitch;
      data.AeroTSlope.CT0_Yaw];
CdA
q = 1/2*atmos.rho*norm(flow)^2;
T=q*(k*[r1;r2;r3]+CT0);

%Acceleration (this model don't produce linear acceleration)
A=[0;0;0];

%Extra state variables (this model doesn't need extra state variables)
E=zeros(1,length(x)-13);

end