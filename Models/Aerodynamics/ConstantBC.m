function [A,T,E] = ConstantBC (t,x,data)
% ConstantBC is a model to compute the drag of the spacecraft only
% using its ballistic coefficient BC = mass/(Cd*A).
% This model assumes that there is no atmospheric co-rotation or winds.
%
% Parameters:
%   data.ConstantBC.BC -> Spacecraft ballistic coefficient BC = mass/(Cd*A)
%
%   data.ConstantBC.atmos_model -> Function handle specifying the
%   atmospheric model. The output of the atmospheric model must comply with
%   the standard format and at least contain the atmos.rho field specifying
%   the atmospheric density in kg/m3.
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )


%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%
% Convert position and velocity to EARTH Reference frame
REF_EARTH=cspice_spkezr( 'EARTH', t, data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody);
EARTH_SC=x(1:6)-REF_EARTH(1:6)*1e3;

%Flow is the velocity
flow = EARTH_SC(4:6);

%Compute the atmospheric properties
atmos = data.ConstantBC.atmos_model(t,x,data);

%Compute drag acceleration
q = 1/2*atmos.rho*norm(flow)^2; %Dynamic pressure
A = -q*flow/norm(flow)/data.ConstantBC.BC;

%Torque (this model doesn't create any torque)
T=[0,0,0]';

%Extra state variables (this model doesn't need extra state variables)
E=zeros(1,length(x)-13);

end