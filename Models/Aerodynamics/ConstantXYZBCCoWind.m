function [A,T,E] = ConstantXYZBCCoWind(t,x,data)
% ConstantXYZBCCoWind is a model to compute the aerodynamic force on the 
% spacecraft only using its ballistic coefficient BC = mass/(Cd*A).
% This model assumes that the atmosphere co-rotates with the Earth and 
% allows for the possibility to include wind.
%
% Parameters:
%   data.ConstantXYZBCCoWind.BC -> Spacecraft ballistic coefficient vector 
%   (3x1) BC = (Cd*A)/mass in velocity (BC1) perpendicular to the orbit 
%   plane (BC2) and towards the centre of the planet (completing the triad)
%   (BC3).
%
%   data.ConstantXYZBCCoWind.atmos_model -> Function handle specifying the
%   atmospheric model. The output of the atmospheric model must comply with
%   the standard format and at least contain the atmos.rho field specifying
%   the atmospheric density in kg/m3.
%
%   data.ConstantXYZBCCoWind.wind_model -> Function handle specifying the wind
%   model. The output must be a 3x1 vector containing the wind in PCPF
%   coordinates.
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )
%
%   data.GravEGM96.EarthFrame -> Earth reference frame to convert to ECEF.
%   It can be the 'IAU_EARTH' or the more precise 'ITRF93' frame.


%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%
%Compute flow velocity
flow = Flow_Co_Wind(t,x,data,'ConstantXYZBCCoWind');

%Compute the atmospheric properties
atmos = data.ConstantXYZBCCoWind.atmos_model(t,x,data);

%Compute aerodynamic acceleration
q = 1/2*atmos.rho*norm(flow)^2; %Dynamic pressure
A1 = -q*flow/norm(flow)*data.ConstantXYZBCCoWind.BC(1);
y = cross(x(1:3),flow)/norm(cross(x(1:3),flow));
A2 = q*y*data.ConstantXYZBCCoWind.BC(2);
z = cross(flow,y)/norm(cross(flow,y));
A3 = q*z*data.ConstantXYZBCCoWind.BC(3);
A = A1+A2+A3;

%Torque (this model doesn't create any torque)
T=[0,0,0]';

%Extra state variables (this model doesn't need extra state variables)
E=zeros(1,length(x)-13);

end