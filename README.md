# AstroLab

AstroLab is a modular and versatile Open Source orbit propagator in MATLAB for all of us to enjoy.
The main features of AstroLab are:

* 3dof or 6dof propagator. Select if you want to propagate the attitude or not.
* Use of the [NASA SPICE](http://naif.jpl.nasa.gov/naif/toolkit.html) toolkit to compute planetary ephemerides.
* Different environmental and dynamic models. Select the environment/force/torque models you want to apply (gravity, drag, atmosphere, winds, solar radiation pressure, gravity gradient, ...).
* Capable of propagating extra state variables (used for hysteresis rods, filters, control loops, ...).

## Installation

Installing AstroLab is very simple, just copy it somewhere in your Matlab path, then execute the `install.m` script to install the SPICE toolkit. After that you can start crunching orbits.

## Dependencies

Currently AstroLab requires some Matlab toolboxes (i.e. the Aerospace toolbox).

## Usage

Check the documentation inside the functions, and check the 'Test' folder for usage examples.

## Contributing

AstroLab is an open source project that needs your contributions. Feel free to fork it an modify it, but make sure to report any bugs you find.

## Legal Boring Stuff

Copyright 2012-2013 Cranfield University
Written by Josep Virgili

This file is part of the AstroLab toolkit

AstroLab is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

AstroLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.