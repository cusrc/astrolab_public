function [value,isterminal,direction] = EvArgLat(t,x,data)
% EvArgLat is an event handler that stops the integration when a certain
% argument of latitude (EARTH) is reached.
%
% Parameters:
%   data.EvArgLat.u -> Argument of latitude in deg to stop integration.
%   
%   data.EvArgLat.EarthFrame -> Earth reference frame to convert to ECEF.
%   It can be the 'IAU_EARTH' or the more precise 'ITRF93' frame.

%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

% Convert position and velocity to EARTH Reference frame
REF_EARTH=cspice_spkezr( 'EARTH', t, data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody);
EARTH_SC=x(1:6)-REF_EARTH(1:6)*1e3;

% Convert position to ECEF (ITRF93) using the SPICE routine
Rot = cspice_pxform( data.prop.Reference.Frame, data.EvArgLat.EarthFrame, t);
r=Rot*EARTH_SC(1:3);
v=Rot*EARTH_SC(4:6);

%Ascending node vector
h = cross(v,-r); %Angular momentum vector
n = cross([0;0;1],h); %Vector pointing towards the ascending node

%Argument of latitude
u = real(acosd(dot(n,r)/norm(n)/norm(r)));

%Only positive arguments of latitude
if dot(n,v)>0
    u=360-u;
end

% Event values
value = u - data.EvArgLat.u ; 
isterminal = 1; %When the altitude is reached stop integration
direction = 1; %Only positive

end