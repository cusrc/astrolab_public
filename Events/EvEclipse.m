function [value,isterminal,direction] = EvEclipse(t,y,data)
% EvEclipse is an event handler that marks when the spacecraft enters and 
% exits an eclipse.  
%
% Parameters:
%   data.EvEclipse.EclipsingBody -> Eclipsing body (Earth, Jupiter, ... )
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )


%--- Copyright notice ---%
% Copyright 2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%
% Convert position and velocity to EARTH Reference frame
REF_BODY=cspice_spkezr( data.EvEclipse.EclipsingBody, t, data.prop.Reference.Frame,'NONE', data.prop.Reference.CentreBody);
BODY_SC=y(1:3)-REF_BODY(1:3)*1e3;

%Normalized Sun vector
BODY_SUN=cspice_spkezr( 'SUN', t, data.prop.Reference.Frame,'NONE', data.EvEclipse.EclipsingBody );
BODY_SUN=BODY_SUN(1:3)*1e3;

%Eclipsing body radius
BODY_R = cspice_bodvrd( data.EvEclipse.EclipsingBody, 'RADII', 3 )*1e3;

%Check if spacecraft is in eclipse
eclipse = +check_eclipse(BODY_SC,BODY_SUN,BODY_R(1));


%--- Event values ---%
value = eclipse; 
isterminal = 0; %When it enters or exit eclipse do not stop the integration
direction = 0; %Both directions


end