function [value,isterminal,direction] = EvAtt(t,x,data)
% EvAtt is an event handler that stops integration when the spacecraft 
% achieves a certain attitude w.r.t the flow. This event is used to control
% the spin when DDsat counter-rotates its panels.
%
% Parameters:
%   data.EvAtt.wind_model -> Function handle specifying the wind
%   model. The output must be a 3x1 vector containing the wind in PCPF
%   coordinates.
%
%   data.EvAtt.r1-> Target roll angle.
%
%   data.EvAtt.r2 -> Target pitch angle.
%
%   data.EvAtt.r2 -> Target yaw angle.
%
%   data.prop.Reference.CentreBody -> Reference frame centre body (EARTH, Solar
%   System Barycenter (SSB), MARS, ...).
%
%   data.prop.Reference.Frame -> Reference frame ( J2000 )
%
%   data.EvAtt.EarthFrame -> Earth reference frame to convert to ECEF.
%   It can be the 'IAU_EARTH' or the more precise 'ITRF93' frame.

%--- Copyright notice ---%
% Copyright 2012-2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%--- CODE ---%

%--- Attitude in flow reference frame ---%
[r1,r2,r3] = Attitude_Flow(t,x,data,'EvAtt');

%--- Event values ---%
value = [r1-data.EvAtt.r1,r2-data.EvAtt.r2,r3-data.EvAtt.r3]; 
isterminal = [1,1,1]; %When the attitude is reached stop integration
direction = [0,0,0]; %Both directions


end