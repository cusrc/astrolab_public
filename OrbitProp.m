function [data]=OrbitProp(data)
% General purpose modular orbit propagator main function.
%
% Usage:
%   [data] = OrbitProp(data)
%
% Where:
%   data -> Is the main structure used to pass data in and out. The main
%   fields of data are:
%
%       data.prop -> Propagator data, used to pass general propagation data.
%
%           data.prop.x0 -> Initial state vector for the propagation. See
%           below for the formatting and units of the state vector.
%
%           data.prop.t -> Time vector for the propagation in s. It should
%           contain at least 2 elements. If data.prop.t has more than two
%           elements the solutions will be provided in those times. If
%           data.prop.t contains only 2 elements then the propagator will
%           provide solutions at its discretion.
%
%           data.prop.sc.I -> Spacecarft inertia matrix [3x3] in kg*m^2.
%           Ignore this field if the initial state vector doesn't contain
%           an initial attitude.
%
%           data.prop.Reference.CentreBody -> Reference centre body for
%           propagation. Check the documentation to see which ones are
%           currently accepted.
%
%           data.prop.Reference.Frame -> Reference frame for the
%           propagation. Check the documentation to see which ones are
%           currently accepted.
%
%           data.prop.waitbar -> (Optional) Set it to True to activate the 
%           waitbar. By default is set to False.
%
%           data.prop.reserved -> Reserved fields to carry internal data.
%           Data passed in this field may be overwritten.
%
%       data.models -> An array of function handlers defining the dynamic 
%       models to be applied. Check the models folder to see which ones are
%       available.
%
%       data.model_name.field -> Configuration of the models. Please check 
%       the models documentation on how to configure the different models.
%
%       data.event -> A function handler defining the event to be applied.
%       This can be used to terminate the integration or to mark where 
%       certain special events happen during the integration. Check the 
%       events folder to see which ones are available. 
%
%       data.event_name.field -> Configuration of the events. Please check 
%       the events documentation on how to configure the different events.
%
%       data.sol -> Contains the solution of the propagation it contains
%       the following fields:
%
%           data.sol.t -> Times of the state vector solution.
%
%           data.sol.x.centre_body.reference_frame -> State vectors of the 
%           solution. Specifying which centre body and reference frame the
%           state vector refers allows to include the state vectors in
%           different centre bodies and reference frames in the same data
%           structure.
%
%           data.sol.te -> If events are specified this field is returned
%           containing the times at which events, if any, occurred.
%
%           data.sol.xe.centre_body.reference_frame -> State vectors at the
%           events. Specifying which centre body and reference frame the
%           state vector refers allows to include the state vectors in
%           different centre bodies and reference frames in the same data
%           structure.
%
%           data.sol.ie -> Indices of the events functions at the events.
%
%
% State vectors have the following format:
%       x = [x,y,z,Vx,Vy,Vz,q1,q2,q3,q4,w1,w2,w3]
%
% Where:
%       xyz -> Position in m on a ECEF frame.
%       Vxyz -> Velocity in m/s.
%       q1234 -> Attitude quaternion. The reference frame is ECEF and the
%       Euler sequence is E123. q1 is the scalar value. 
%       w123 -> Angular rate in rad/s in the body reference frame
%
% Attitude can be ignored passing only the linear parameters as:
% data.prop.x0 = [x,y,z,Vx,Vy,Vz]
%
% Examples are provided in the Examples folder.

%--- Copyright notice ---%
% Copyright 2012-2013 Cranfield University
% Written by Josep Virgili
%
% This file is part of the AstroLab toolkit
%
% AstroLab is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AstroLab is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AstroLab.  If not, see <http://www.gnu.org/licenses/>.

%---- CODE ----%

%---- Check initial state vector ---%

%Convert initial state to column vector
data.prop.x0=data.prop.x0(:);

%Check size of initial state column vector
if length(data.prop.x0)<6
    %Initial state vector doesn't contain minimum number of elements
    %Throw error
    error('data.prop.x0 must be have at least x,y,z,Vx,Vy,Vz');
elseif length(data.prop.x0)==6
    %Ignoring attitude
    data.prop.reserved.attitude = false;
elseif length(data.prop.x0)<13
    %The size of the initial state vector is incorrect.
    %Throw error
    error('The initial vector size doesn''t match with x,y,z,Vx,Vy,Vz or x,y,z,Vx,Vy,Vz,q1,q2,q3,q4,w1,w2,w3'); 
elseif length(data.prop.x0)==13
    %Propagating attitude
    data.prop.reserved.attitude = true;
end

%--- Check for models ---%
if isfield(data,'models');
    %The field exists
    %Convert to column cell array
    data.models=data.models(:);
else
    %There are no models specified
    %Throw error
    error('No models specified in data.models');
end

%--- Add waitbar ---%
if isfield(data.prop,'waitbar');
        if data.prop.waitbar
            %Waitbar is active (data.prop.reserved.wb contains internbal data of the waitbar)
            data.prop.reserved.wb.waitbar=waitbar(0,'AstroLab. Starting propagation.'); %Create waitbar
            data.prop.reserved.wb.tprop0= now; %Get initial propagation time as now
        end
else
    data.prop.waitbar = false; %Waitbar not active
end

%--- Check for events and configure integrator ---%
if isfield(data,'event');
    %Event function found
    data.prop.reserved.int_options=odeset('RelTol',1e-9,'AbsTol',1e-9,'Event',data.event);
else
    %Event function not found
    data.prop.reserved.int_options=odeset('RelTol',1e-9,'AbsTol',1e-9);
end

%--- Integration ---%
if isfield(data,'event');
    [T,Y,TE,YE,IE]=ode113(@DyEq,data.prop.t,data.prop.x0,data.prop.reserved.int_options,data);
else
    [T,Y]=ode113(@DyEq,data.prop.t,data.prop.x0,data.prop.reserved.int_options,data);
end

%--- Format Solution ---%
data.sol.t=T; %Time
data.sol.x.(data.prop.Reference.CentreBody).(data.prop.Reference.Frame)=Y; %State vector
%Event solutions
if isfield(data,'event');
    data.sol.te=TE; %Events time
    data.sol.xe.(data.prop.Reference.CentreBody).(data.prop.Reference.Frame)=YE'; %Events state vector
    data.sol.ie=IE; %Events indices
end

%--- Close waitbar ---%
try close(data.prop.reserved.wb.waitbar); catch; end;

end